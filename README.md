# CL-Kernel-boss

This is a script that will re-assign the ownership of issues with z stream fix
versions set to the relevant maintainer.

## Configuration via environment variables

| Name                         | Type     | Secret | Required | Description                                                           |
|------------------------------|----------|--------|----------|-----------------------------------------------------------------------|
| `JIRA_URL`                   | url      | no     | yes      | URL of JIRA instance                                                  |
| `JIRA_TOKEN`                 | string   | yes    | yes      | JIRA access token                                                     |
| `OWNERS_YAML_PATH`           | filepath | no     | yes      | path or URL to [owners.yaml]                                          |

## Building 

You can build the container with the command.

```bash
	podman build . -t kernel-boss
```


## Running the script locally.

Setup the JIRA_KEY environment variable, you can get one from logging into JIRA,
accessing your profile, and creating one in the "personal access tokens" tab.

Set it with

$ export JIRA_KEY="SomeCRAZYlongJIRASTRINGPROVIDED"

And then run
$ podman run --env 'JIRA_KEY*' --rm -it

```bash
podman run --rm \
    --pull=newer \
    --env 'JIRA_KEY*' 
    quay.io/rhn-support-wmealing/cl-kernel-boss:main
```


