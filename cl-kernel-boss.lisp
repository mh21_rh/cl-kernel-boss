;;;; cl-kernel-boss.lisp

(in-package #:cl-kernel-boss)

(ql:quickload "cl-jira")
(ql:quickload "jonathan")
(ql:quickload "lparallel")
(ql:quickload "arrows")
(ql:quickload "str")

;; load the jira key
(load "config.lisp")
(cl-jira:set-api-key (uiop:getenv "JIRA_KEY"))
(format t "GOT JIRA KEY: ~a"  (uiop:getenv "JIRA_KEY"))

         ;; as per https://issues.redhat.com/secure/Dashboard.jspa?selectPageId=12345299
         (defparameter find-issues-query "PROJECT = RHEL AND assignee = kernel-mgr and resolution  = Unresolved and filter = \"CK SSG Projects\" AND status not in (Closed) AND filter = \"CK SSG PoolTeams\" AND (labels is EMPTY OR labels not in (RIJ-demo, RIJ-Test))")


         ;; important-cves/important-cves.lisp:  (lparallel:pmapcar 'issue-and-summary issue-list) )
         (defun get-issues-worker ( issue)
           (cl-jira:get-issue (getf issue :|key|)))

(defun get-kernel-mgr-issues ()
  (let ((issue-list (cl-jira:query-with-raw find-issues-query)))
    (lparallel:pmapcar 'get-issues-worker (getf issue-list :|issues|))))

(defun read-staff-json()
  ;; fixme, read from https://gitlab.com/wmealing_rh/json-cleanup/-/raw/main/staff.json?ref_type=heads
  (jonathan:parse
   (uiop:read-file-string
    (asdf:system-relative-pathname "cl-kernel-boss" "staff.json"))))

;;              (cl-jira:issue->set-assignee (cl-jira:get-issue "KMAINT-135") "rhn-support-wmealing")

(defun fixversion-to-jstaff-jsonversion (fixversion)
  (str:substring 5 8 fixversion)
  )

(defun get-stream-owner (version-list version)
  ;; this should only return 1.
  (first
   (remove-if-not (lambda (v)
                    (string= version (getf v :|Stream|))) version-list)
   ))

(defun main()
  (cl-jira:set-api-key (uiop:getenv "JIRA_KEY"))
  (format t "GOT JIRA KEY: ~a"  (uiop:getenv "JIRA_KEY"))
  ;; amount of worker processes.
  (setf lparallel:*kernel* (lparallel:make-kernel 8))

  (let ((issue-list (get-kernel-mgr-issues))
        (staff-json (read-staff-json)))

    (loop for issue in issue-list
          do (progn
               (let ((issue-id (cl-jira:issue->key issue))
                     (previous-assignee (getf (cl-jira:issue->assignee issue) :|displayName|))
                     (fix-version (cl-jira:issue->fixversion issue))
                     (new-assignee (getf
                                    (arrows:some->>
                                      (cl-jira:issue->fixversion issue)
                                      (fixversion-to-jstaff-jsonversion)
                                      (get-stream-owner staff-json)
                                      ) :|Maintainer Jira Login|)))

                 (if (not (eql nil fix-version))
                     (format t "HIT~%")
                     (format t "MISS~%")
                     ;;                     (cl-jira:issue->set-assignee issue new-assignee)
                     ;; (progn
                     ;;   (format t "~s PREV ASSIGNEE: ~s  VERSION : ~s NEW-ASSIGNEE: ~s~%"
                     ;;           issue-id
                     ;;           previous-assignee
                     ;;           fix-version
                     ;;           new-assignee)
                     ;;   (force-output)
                     ;;
                     ))))))

;; (cl-jira:issue->set-assignee (cl-jira:get-issue "KMAINT-135") "nmurray@redhat.com")
