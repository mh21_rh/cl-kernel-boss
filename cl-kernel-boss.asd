;;;; cl-kernel-boss.asd

(asdf:defsystem #:cl-kernel-boss
  :description "Task to re-assign kernel bugs in jira"
  :author "Wade Mealing <wmealing@redhat.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :depends-on (#:cl-jira #:jonathan #:lparallel #:arrows #:str)
  :components ((:file "package")
               (:file "cl-kernel-boss")))
